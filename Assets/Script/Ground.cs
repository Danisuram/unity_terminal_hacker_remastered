﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Component will be added if don't exist & The Component can't be thronw away
[RequireComponent(typeof(BoxCollider2D))]
public class Ground : MonoBehaviour
{
    [SerializeField] private Bird bird;
    [SerializeField] private float speed = 1;
    [SerializeField] private Transform nextPos;
    

    // Update is called once per frame
    void Update()
    {
        //Checking if Bird Null or still not dead
        if(bird == null ||(bird != null && !bird.IsDead()))
        {
            // Make the ground move to the left at the speed of the variable speed
            transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
        }

    }

    //Method for placing game object on next ground position
    public void SetNextGround(GameObject ground)
    {
        //Checking Null value
        if(ground != null)
        {
            //Placing the next ground on nextground position
            ground.transform.position = nextPos.position;
        }
    }

    //Called when a game object comes into contact with another game object
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Make The Bird dead when contact with this game object
        if(bird != null && !bird.IsDead())
        {
            bird.Dead();
        }
    }

}
