﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawner : MonoBehaviour
{
    [SerializeField] private Bird bird;
    [SerializeField] private Pipe pipeUp, pipeDown;
    [SerializeField] private float spawnInterval = 1;
    [SerializeField] public float holeSize = 1f;
    [SerializeField] private float maxMinOffset = 1;
    [SerializeField] private Point point;

    //Variable to contain running coroutine
    private Coroutine CR_Spawn;

    void Start()
    {
        //Start Spawning the pipe
        StartSpawn();
        
    }

    void StartSpawn()
    {
        //Running coroutine IeSpawn() function
        if(CR_Spawn == null)
        {
            CR_Spawn = StartCoroutine(IeSpawn());
        }
    }

    void StopSpawn()
    {
        //Stopping coroutine IeSpawn if already running
        if(CR_Spawn != null)
        {
            StopCoroutine(CR_Spawn);
        }
    }

    void SpawnPipe()
    {
        //Duplicate pipeUp game object and place on the same position with this game object but rotate in 180 degree
        Pipe newPipeUp = Instantiate(pipeUp, transform.position, Quaternion.Euler(0, 0, 180));

        //Activate newPipeUp game object
        newPipeUp.gameObject.SetActive(true);

        //Duplicate game object pipeDown and place on the same position with game object
        Pipe newPipeDown = Instantiate(pipeDown, transform.position, Quaternion.identity);

        //Activate game object newPipeDown
        newPipeDown.gameObject.SetActive(true);

        //Positions the formed pipe so that it has a hole in the middle
        //newPipeUp.transform.position += Vector3.up * (holeSize / 2);
        //newPipeDown.transform.position += Vector3.down * (holeSize / 2);

        newPipeUp.transform.position += Vector3.up * (Random.Range(0.3f, 0.9f));
        newPipeDown.transform.position += Vector3.down * (Random.Range(0.3f, 0.9f));

        //Placing the pipe position that has been formed so that its position adapts to the Sin function
        float y = maxMinOffset * Mathf.Sin(Time.time);
        newPipeUp.transform.position += Vector3.up * y;
        newPipeDown.transform.position += Vector3.up * y;

        Point newPoint = Instantiate(point, transform.position, Quaternion.identity);
        newPoint.gameObject.SetActive(true);
        newPoint.setSize(holeSize);
        newPoint.transform.position += Vector3.up * y;
    }

    IEnumerator IeSpawn()
    {
        while (true)
        {
            //If The Bird die then stop creating new pipe
            if (bird.IsDead())
            {
                StopSpawn();
            }

            //Create new pipe
            SpawnPipe();

            //Waiting for a few second according to the spawn interval
            yield return new WaitForSeconds(spawnInterval);
        }
    }

    
}
