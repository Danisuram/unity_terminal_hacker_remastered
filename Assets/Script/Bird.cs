﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

//Flappy Bird Remastered

public class Bird : MonoBehaviour
{
    //Global Variables
    [SerializeField] private float upForce = 100;
    [SerializeField] private bool isDead;
    [SerializeField] private UnityEvent OnJump, OnDead;
    [SerializeField] private int score;
    [SerializeField] private UnityEvent OnAddPoint;
    [SerializeField] private Text scoreText;

    private Rigidbody2D rigidbody2d;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        //Get Rigidbody2D component when game begin to start
        rigidbody2d = GetComponent<Rigidbody2D>();
        //Get Animator component when game begin to start
        animator = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //Checking bird state if still not dead & left clik on mouse
        if(!isDead && Input.GetMouseButtonDown(0))
        {
            //bird is jumping
            Jump();
        }

    }

    //Function to check if the bird already dead or not
    public bool IsDead()
    {
        return isDead;
    }

    //Make the bird is dead
    public void Dead()
    {
        //Checking if not dead & value onDead not equal Null
        if(!isDead && OnDead != null)
        {
            //Calling all event on OnDead
            OnDead.Invoke();
        }

        //Set Dead varianble into True
        isDead = true;
    }

    void Jump()
    {
        //Checking if Rigidbody Null or not
        if (rigidbody2d)
        {
            //Stop the bird speed when falling down
            rigidbody2d.velocity = Vector2.zero;

            //Add force on Y axis so the bird can jumping
            rigidbody2d.AddForce(new Vector2(0, upForce));
        }

        //Checking Null variable
        if(OnJump != null)
        {
            //Execute all event on OnJump event
            OnJump.Invoke();
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Stop the bird animation when touching other object
        animator.enabled = false;
    }

    public void AddScore(int value)
    {
        //Adding Score Value
        score += value;
        scoreText.text = score.ToString();

        //Checking null value
        if(OnAddPoint != null)
        {
            //Calling all event on OnAddPoint
            OnAddPoint.Invoke();
        }
    }
}
