﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour
{

    [SerializeField] private Bird bird;
    [SerializeField] private float speed = 1;


    // Update is called once per frame
    void Update()
    {
        //Checking if The Bird is not dead
        if (!bird.IsDead())
        {
            //Make The Pipe move to the left side with certain speed
            transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
        }
        
    }

    //Make The Bird die on contact and drop it to ground if hit on top of the collider
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Bird bird = collision.gameObject.GetComponent<Bird>();

        //Checking null value
        if (bird)
        {
            //Get Collider component on game object
            Collider2D collider = GetComponent<Collider2D>();

            //Checking null variable or isn't
            if (collider)
            {
                //Non-Activate collider
                collider.enabled = false;
            }

            //The Bird is dead
            bird.Dead();
        }
    }
}
