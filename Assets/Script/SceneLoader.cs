﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadScene(string name)
    {
        //Checking if name not null or empty
        if (!string.IsNullOrEmpty(name))
        {
            //Open scene with name as variable name
            SceneManager.LoadScene(name);
        }
    }
}
