﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class GroundSpawner : MonoBehaviour
{
    //Contain ground reference that want to create
    [SerializeField] private Ground groundRef;

    //Contain previous ground
    private Ground prevGround;

    //This method will create new Ground game object
    private void SpawnGround()
    {
        //Checking Null Variable
        if(prevGround != null)
        {
            //duplicate Groundref
            Ground newGround = Instantiate(groundRef);

            //Activate game object
            newGround.gameObject.SetActive(true);

            //Placing new ground with nextground position from prevground for position parallel with previous ground
            prevGround.SetNextGround(newGround.gameObject);
        }
    }

    //This method will be called when there is another game object that has collider component come out from collider area
    private void OnTriggerExit2D(Collider2D collision)
    {
        //Searching ground component from object comes out from trigger area
        Ground ground = collision.GetComponent<Ground>();

        //Checking Null Variable
        if (ground)
        {
            //Fill the prevGround variable
            prevGround = ground;

            //Create new Ground
            SpawnGround();
        }
    }
}
