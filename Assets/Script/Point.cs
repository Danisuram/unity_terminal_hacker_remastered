﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Point : MonoBehaviour
{
    [SerializeField] private Bird bird;
    [SerializeField] private float speed = 1;

    // Update is called once per frame
    void Update()
    {
        //Checking if The bird is dead or not
        if (!bird.IsDead())
        {
            //Moving game object on left site with certain speed
            transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
        }
        
    }

    public void setSize(float size)
    {
        //Get BoxCollider2D component
        BoxCollider2D collider = GetComponent<BoxCollider2D>();
        //Checking null variable
        if(collider != null)
        {
            //resize the collider according with paramater
            collider.size = new Vector2(collider.size.x, size);
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        //Get bird component
        Bird bird = collision.gameObject.GetComponent<Bird>();
        
        //Add score if bird not null and bird not dead
        if(bird && !bird.IsDead())
        {
            bird.AddScore(1);
        }
    }
}
